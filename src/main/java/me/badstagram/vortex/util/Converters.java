package me.badstagram.vortex.util;

public class Converters {

    public static int toDays(int count) {
        return count * 60 * 60 * 24;
    }

    public static int toHours(int count) {
        return count * 60 * 60;
    }
}

