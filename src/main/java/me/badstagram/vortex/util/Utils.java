package me.badstagram.vortex.util;


import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.enums.ConfigOption;
import me.badstagram.vortex.core.enums.PunishmentType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import javax.annotation.Nullable;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

public class Utils {

    private Utils() {
        throw new IllegalStateException("Util class");
    }

    public static String getUptime() {

        final long duration = ManagementFactory.getRuntimeMXBean().getUptime();

        final long years = duration / 31104000000L;
        final long months = duration / 2592000000L % 12;
        final long days = duration / 86400000L % 30;
        final long hours = duration / 3600000L % 24;
        final long minutes = duration / 60000L % 60;
        final long seconds = duration / 1000L % 60;

        String uptime = "";
        uptime += years == 0 ? "" : years + " Year" + (years > 1 ? "s" : "") + ", ";
        uptime += months == 0 ? "" : months + " Month" + (months > 1 ? "s" : "") + ", ";
        uptime += days == 0 ? "" : days + " Day" + (days > 1 ? "s" : "") + ", ";
        uptime += hours == 0 ? "" : hours + " Hour" + (hours > 1 ? "s" : "") + ", ";
        uptime += minutes == 0 ? "" : minutes + " Minute" + (minutes > 1 ? "s" : "") + ", ";
        uptime += seconds == 0 ? "" : seconds + " Second" + (seconds > 1 ? "s" : "") + ", ";

        uptime = replaceLast(uptime, ", ", "");
        uptime = replaceLast(uptime, ",", " and");
        return uptime;


    }

    public static String replaceLast(final String text, final String regex, final String replacement) {
        return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
    }

    public static void addPunishmentToDb(String offenderId, String moderatorId, @Nullable String reason, String guildId, PunishmentType type, @Nullable String length, MessageReceivedEvent event) {

        try (Connection conn = DatabaseUtils.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(String.format("INSERT INTO '%s' (offender_id, moderator_id, reason, type, remove_at, state) VALUES (?,?,?,?,?,?)", guildId));) {

            preparedStatement.setString(1, offenderId);
            preparedStatement.setString(2, moderatorId);
            preparedStatement.setString(3, reason);
            switch (type) {
                case BAN:
                    preparedStatement.setString(4, "Ban");
                    break;
                case KICK:
                    preparedStatement.setString(4, "Kick");
                    break;
                case MUTE:
                    preparedStatement.setString(4, "Mute");
                    break;
            }

            if (length != null) {
                String duration = length.toLowerCase();
                int time = Integer.parseInt(duration.replaceAll("[s|m|d|w|mo|y]", ""));
                long timestamp;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(Date.from(Instant.now()));

                calendar.add(Calendar.MONTH, time);
                timestamp = calendar.getTime().toInstant().toEpochMilli();

                preparedStatement.setString(5, String.valueOf(timestamp));
            } else {
                preparedStatement.setString(5, "null");
            }
            preparedStatement.setString(6, "Active");
            preparedStatement.execute();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event);
        }

    }

    public static void setConfigSetting(String guildId, String value, ConfigOption option) {
        PreparedStatement preparedStatement = null;
        try (Connection conn = DatabaseUtils.getConnection()) {
            switch (option) {
                case MOD_LOG:
                    preparedStatement = conn.prepareStatement(String.format("UPDATE \"%s\" SET mod_log = ?", guildId));
                    break;
                case MUTED_ROLE:
                    preparedStatement = conn.prepareStatement(String.format("UPDATE \"%s\" SET muted_role = ?", guildId));
                    break;
                case PUNISH_LOG:
                    preparedStatement = conn.prepareStatement(String.format("UPDATE \"%s\" SET punish_log = ?", guildId));
                    break;
                default:
                    return;
            }

            preparedStatement.setString(1, value);
            preparedStatement.execute();
        } catch (SQLException e) {
            ErrorHandler.handleExceptionNoMessage(e);
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
            } catch (SQLException e) {
                ErrorHandler.handleExceptionNoMessage(e);
            }
        }
    }

    public static String getConfigSetting(String guildId, ConfigOption option) {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try (Connection conn = DatabaseUtils.getConnection();) {

            switch (option) {
                case MOD_LOG:
                    preparedStatement = conn.prepareStatement(String.format("SELECT %s FROM \"%s\" WHERE ? = ?", guildId, "mod_log"));
                    rs = preparedStatement.executeQuery();
                    return rs.getString("mod_log");

                case MUTED_ROLE:
                    preparedStatement = conn.prepareStatement(String.format("SELECT %s FROM \"%s\" WHERE ? = ?", guildId, "muted_role"));
                    rs = preparedStatement.executeQuery();
                    return rs.getString("muted_role");

                case PUNISH_LOG:
                    preparedStatement = conn.prepareStatement(String.format("SELECT %s FROM \"%s\" WHERE ? = ?", guildId, "punish_log"));
                    rs = preparedStatement.executeQuery();
                    return rs.getString("punish_log");
                default:
                    return "";
            }

        } catch (SQLException e) {
            ErrorHandler.handleExceptionNoMessage(e);
        } finally {
            try {
                if (rs != null) rs.close();

            } catch (SQLException e) {
                ErrorHandler.handleExceptionNoMessage(e);
            } finally {
                try {

                    if (preparedStatement != null) preparedStatement.close();
                } catch (SQLException e) {
                    ErrorHandler.handleExceptionNoMessage(e);
                }
            }
        }
        return "";
    }

    public static void log(Class<?> clazz, Object message, Level level) {
        final Logger log = LoggerFactory.getLogger(clazz.getName());
        switch (level) {
            case INFO:

                log.info("{}", message);
                break;
            case WARN:
                log.warn("{}", message);
                break;
            case DEBUG:
                log.debug("{}", message);
                break;
            case ERROR:
                log.error("{}", message);
                break;
            default:
                return;
        }
    }

    public static String decodeBase64(String base64EncodedText) {
        byte[] bytes = Base64.getDecoder().decode(base64EncodedText);

        return new String(bytes);

    }
}



