package me.badstagram.vortex.util;

import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Config;
import me.badstagram.vortex.core.ErrorHandler;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.sql.*;

public class DatabaseUtils {
    private static Connection connection = null;

    private DatabaseUtils() {
    }

    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {

            connection = DriverManager.getConnection(Config.get("db_url"), Config.get("db_user"), Config.get("db_password"));
        }
        return connection;
    }

    public static int getMoney(String userId, String guildId, @Nullable CommandEvent event) {
        ResultSet rs = null;
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement preparedStatement = con.prepareStatement("SELECT hand FROM economy WHERE user_id = ? AND guild_id = ?")) {

            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, guildId);



            rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return rs.getInt("hand");
            }


        } catch (Exception e) {
            if (event != null) {
                ErrorHandler.handleException(e, event.getEvent());
            } else {
                ErrorHandler.handleExceptionNoMessage(e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                if (event != null) {
                    ErrorHandler.handleException(e, event.getEvent());
                } else {
                    ErrorHandler.handleExceptionNoMessage(e);
                }
            }
        }
        return -1;
    }
}


