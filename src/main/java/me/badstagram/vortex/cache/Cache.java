package me.badstagram.vortex.cache;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.entities.Message;
import org.bson.Document;

public class Cache {
    static MongoClient mongoClient = Vortex.getMongoClient();
    static MongoDatabase cache = mongoClient.getDatabase("Cache");
    static MongoCollection<Document> messages = cache.getCollection("Messages");

    public static MongoCollection<Document> getMessages() {return messages;}
    public static void addMessage(Message message) {

        Document content = new Document()
                .append("content", message.getContentRaw())
                .append("guildId", message.getGuild().getId())
                .append("author", message.getAuthor().getId())
                .append("newConent", null)
                .append("deleted", false);


        Document finalDoc = new Document(message.getId(), content);

        messages.insertOne(finalDoc);
    }


    public static void clear() {
        messages.deleteMany(new Document());
    }

}

