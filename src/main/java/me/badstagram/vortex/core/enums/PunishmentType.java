package me.badstagram.vortex.core.enums;

public enum PunishmentType {
    BAN,
    KICK,
    MUTE
}

