package me.badstagram.vortex.core.enums;

public enum Module {
    ADMIN,
    FUN,
    HELP,
    INFO,
    MODERATION
}
