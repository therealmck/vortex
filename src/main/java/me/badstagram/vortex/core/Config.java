package me.badstagram.vortex.core;

import io.github.cdimascio.dotenv.Dotenv;

import javax.annotation.Nonnull;

public class Config {
    private Config(){}

    public static String get(@Nonnull final String key) {
        Dotenv env = Dotenv.load();

        String value = env.get(key.toUpperCase());

        if (value == null || value.equals("")) {
            throw new IllegalArgumentException(key + " was not found in the .env file.");
        }

        return value;
    }
}

