package me.badstagram.vortex.core.listeners;

import me.badstagram.vortex.automod.AutoMod;
import me.badstagram.vortex.cache.Cache;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class OnGuildMessageReceived extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@Nonnull final GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        if (event.getAuthor().isBot() || message.isWebhookMessage()) {
            return;
        }


        if (!event.getChannel().isNSFW()) {
            AutoMod.moderateAttachments(message.getAttachments(), event);
        }


        try {
            File file = new File("./copypastas.json");
            String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            JSONObject object = new JSONObject(content);
            JSONArray copypastas = object.getJSONArray("copypastas");

            for (Object obj : copypastas) {
                String copypasta = "";
                if (obj instanceof String) {
                    copypasta = (String) obj;
                }

                if (message.getContentRaw().toLowerCase().contains(copypasta.replaceAll("\\r", "").toLowerCase())) {
                    event.getChannel().sendMessage(event.getAuthor().getAsMention() + ", Don't post copypastas.").queue();
                    message.delete().reason("[Anti-spam] Copypasta").queue();
                    return;
                }
            }
        } catch (Exception e) {
            ErrorHandler.handleException(e, event);
        }

        //Cache.addMessage(message);

        if (!message.getMentionedMembers().isEmpty() && message.getMentionedMembers().get(0).getUser().equals(event.getJDA().getSelfUser())) {
            User developer = event.getJDA().retrieveUserById(Vortex.getCommandClient().getOwnerId()).complete();
            event.getChannel().sendMessageFormat("Hi, I am a bot made with <3 by %s. My prefix is `%s`", developer.getAsTag(), Vortex.getCommandClient().getPrefix()).queue();
        }
    }
}


