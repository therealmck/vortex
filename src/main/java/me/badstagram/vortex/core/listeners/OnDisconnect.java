package me.badstagram.vortex.core.listeners;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import me.badstagram.vortex.core.Config;
import net.dv8tion.jda.api.events.DisconnectEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.CloseCode;

import javax.annotation.Nonnull;

public class OnDisconnect extends ListenerAdapter {

    @Override
    public void onDisconnect(@Nonnull DisconnectEvent event) {
        CloseCode closeCode = event.getCloseCode();
        int code = closeCode == null ? 4000 : closeCode.getCode();
        String reason = closeCode == null ? "Unknown" : closeCode.getMeaning();
        String reconnect = closeCode == null ? "Unknown" : String.valueOf(closeCode.isReconnect());

        WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
        builder.setTitle(new WebhookEmbed.EmbedTitle("Shard Disconnected!", null));
        builder.setColor(0xFF0000);
        builder.addField(new WebhookEmbed.EmbedField(false, "Close Code", String.valueOf(code)));
        builder.addField(new WebhookEmbed.EmbedField(false, "Shard ID", "0"));
        builder.addField(new WebhookEmbed.EmbedField(false, "Reason", reason));
        builder.addField(new WebhookEmbed.EmbedField(false, "Will reconnect", reconnect));

        WebhookMessageBuilder webhookMessageBuilder = new WebhookMessageBuilder();
        webhookMessageBuilder.addEmbeds(builder.build());
        WebhookClient client = WebhookClient.withUrl(Config.get("status_log_webhook"));
        client.send(webhookMessageBuilder.build()).thenAccept(ignored -> client.close());
    }
}
