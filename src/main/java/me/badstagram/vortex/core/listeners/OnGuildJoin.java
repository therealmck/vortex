package me.badstagram.vortex.core.listeners;

import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class OnGuildJoin extends ListenerAdapter {
    @Override
    public void onGuildJoin(@Nonnull final GuildJoinEvent event) {
        Vortex.getApi().getPresence().setActivity(Activity.watching(String.format("%d guilds", Vortex.getApi().getGuilds().size())));
    }
}


