package me.badstagram.vortex.core.listeners;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandClient;
import me.badstagram.vortex.cache.Cache;
import me.badstagram.vortex.core.Config;

import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class OnReady extends ListenerAdapter {
    @Override
    public void onReady(@Nonnull final ReadyEvent event) {
        Logger logger = LoggerFactory.getLogger(this.getClass());

        JDA jda = event.getJDA();
        logger.info("{} is online!", jda.getSelfUser().getAsTag());
        CommandClient commandClient = Vortex.getCommandClient();

        for (Command c : commandClient.getCommands()) {
            logger.info("Command {} loaded!", c.getName());
        }
        logger.info("{} commands loaded!", commandClient.getCommands().size());

        jda.getPresence().setActivity(Activity.watching(String.format("%d guilds", jda.getGuilds().size())));
        jda.getPresence().setStatus(OnlineStatus.ONLINE);


        WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
        builder.setTitle(new WebhookEmbed.EmbedTitle("Bot Online!", null));
        builder.setColor(0x00FF00);

        WebhookMessageBuilder webhookMessageBuilder = new WebhookMessageBuilder();
        webhookMessageBuilder.addEmbeds(builder.build());
        WebhookClient client = WebhookClient.withUrl(Config.get("status_log_webhook"));
        client.send(webhookMessageBuilder.build()).thenAccept(ignored -> client.close());


    }
}

