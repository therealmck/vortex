package me.badstagram.vortex.core.listeners;

import net.dv8tion.jda.api.events.RawGatewayEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.data.DataObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class OnRawGateway extends ListenerAdapter {

    @Override
    public void onRawGateway(@Nonnull RawGatewayEvent event) {
        DataObject dataPackage = event.getPackage();
        int op = dataPackage.getInt("op");
        String t = dataPackage.getString("t");

        Logger logger = LoggerFactory.getLogger(this.getClass());

        logger.debug("Received event: {}, OP code: {}", t, op);

    }
}
