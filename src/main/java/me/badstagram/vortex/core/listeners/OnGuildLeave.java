package me.badstagram.vortex.core.listeners;

import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class OnGuildLeave extends ListenerAdapter {
    @Override
    public void onGuildLeave(@Nonnull final GuildLeaveEvent event) {
        Vortex.getApi().getPresence().setActivity(Activity.watching(String.format("%d guilds", Vortex.getApi().getGuilds().size())));
    }
}

