package me.badstagram.vortex.core;

import com.jagrosh.jdautilities.command.CommandEvent;
import io.sentry.Sentry;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

public class ErrorHandler {
    private ErrorHandler() {
    }

    /**
     * Logs a {@link Throwable} to sentry and informs the user of the error.
     *
     * @param thr   The Exception
     * @param event The event
     * @author badstagram
     */
    public static void handleException(Throwable thr, GuildMessageReceivedEvent event) {
        Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
        logger.error(thr.getMessage());
        // Inform user there was an exception.
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Command raised an exception.");
        eb.setDescription(MarkdownUtil.monospace(thr.getMessage()).replace(Config.get("vpsIp"), "[Redacted]"));
        eb.setColor(new Color(255, 0, 0));
        String[] typeSplit = thr.getClass().getName().split("\\.");
        eb.setFooter(String.format("This exception has been reported to my developer. | For support, join our support guild: %s | %s", Constants.SUPPORT_SERVER, typeSplit[typeSplit.length - 1]));
        event.getChannel().sendMessage(eb.build()).queue();

        //Log exception to sentry
        Sentry.init(Config.get("sentry"));

        Sentry.getContext().recordBreadcrumb(
                new BreadcrumbBuilder()
                        .withData("Message", event.getMessage().getContentRaw())
                        .withData("Guild Id", event.getGuild().getId())
                        .withData("Guild Name", event.getGuild().getName())
                        .setLevel(Breadcrumb.Level.ERROR).build()
        );
        Sentry.getContext().setUser(
                new UserBuilder()
                        .setUsername(event.getAuthor().getAsTag()).build()
        );


        Sentry.getContext().addTag("Build", Vortex.getReleaseType());
        Sentry.capture(thr);
    }

    /**
     * Logs a {@link Throwable} to sentry and informs the user of the error.
     *
     * @param thr   The Exception
     * @param event The event
     * @author badstagram
     */
    public static void handleException(Throwable thr, MessageReceivedEvent event) {
        Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
        logger.error(thr.getMessage());
        // Inform user there was an exception.
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Command raised an exception.");
                    eb.setDescription(MarkdownUtil.monospace(thr.getMessage()).replace(Config.get("vpsIp"), "[Redacted]"));
        eb.setColor(new Color(255, 0, 0));
        String[] typeSplit = thr.getClass().getName().split("\\.");
        eb.setFooter(String.format("This exception has been reported to my developer. | For support, join our support guild: %s | %s", Constants.SUPPORT_SERVER, typeSplit[typeSplit.length - 1]));
        event.getChannel().sendMessage(eb.build()).queue();

        //Log exception to sentry
        Sentry.init(Config.get("sentry"));

        Sentry.getContext().recordBreadcrumb(
                new BreadcrumbBuilder()
                        .withData("Message", event.getMessage().getContentRaw())
                        .withData("Guild Id", event.getGuild().getId())
                        .withData("Guild Name", event.getGuild().getName())
                        .setLevel(Breadcrumb.Level.ERROR).build()
        );
        Sentry.getContext().setUser(
                new UserBuilder()
                        .setUsername(event.getAuthor().getAsTag()).build()
        );


        Sentry.getContext().addTag("Build", Vortex.getReleaseType());
        Sentry.capture(thr);
    }


    /**
     * Logs a {@link Throwable} to sentry but doesn't inform the user.
     *
     * @param thr The {@link Throwable} to log
     * @author badstagram
     */
    public static void handleExceptionNoMessage(Throwable thr) {
        //Log exception to sentry
        Sentry.init(Config.get("sentry"));
        Sentry.capture(thr);

        Logger log = LoggerFactory.getLogger(ErrorHandler.class);
        log.error(thr.getMessage(), thr);
    }

    /**
     * Informs the user they did not use the correct syntax.
     *
     * @param event The event
     * @author badstagram
     */
    public static void invalidSyntax(CommandEvent event) {

        MessageEmbed embed = new EmbedBuilder()
                .setTitle("There was an error running that command.")
                .setDescription("`Invalid syntax`")
                .setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER))
                .build();

        event.reply(embed);
    }
}


