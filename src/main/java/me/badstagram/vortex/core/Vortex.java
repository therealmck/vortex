package me.badstagram.vortex.core;

import com.jagrosh.jdautilities.command.CommandClient;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import me.badstagram.vortex.core.listeners.*;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import net.explodingbush.ksoftapi.KSoftAPI;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Vortex {
    private static final EventWaiter waiter = new EventWaiter();
    private static final Logger logger = LoggerFactory.getLogger(Vortex.class);
    private static final Map<Long, Long> afkUsers = new HashMap<>();
    private static JDA api = null;
    private static KSoftAPI kSoftAPI = null;
    private static OkHttpClient client = null;
    private static CommandClient commandClient = null;
    private static CommandClientBuilder cmdClientBuilder = null;
    private static MongoClient mongoClient = null;
    private static Random random = null;

    public static void main(String[] args) {
        
        Map<Integer, String> lol = null;
        System.out.println(lol.get(0));


        boolean devMode = false;
        try {

            cmdClientBuilder = new CommandClientBuilder()
                    .useHelpBuilder(false)
                    .setHelpConsumer(null)
                    .setHelpWord(null)
                    .setPrefix(devMode ? "--" : "-")
                    .setOwnerId("264168020862042113")
                    .setEmojis(":white_check_mark:", ":warning:", ":x:");

            commandClient = CommandUtil.addCommands(cmdClientBuilder).build();
            api = login(Config.get(devMode ? "discord_devel" : "discord")).awaitReady();



        } catch (Exception e) {
            logger.error("There was an error and Vortex was unable to start up. Check sentry for more info.");

            ErrorHandler.handleExceptionNoMessage(e);
            System.exit(-1);
        }
    }

    protected static JDA login(String token) throws LoginException, InterruptedException {
        return JDABuilder.create(token, EnumSet.of(GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_EMOJIS, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES))
                .disableCache(CacheFlag.VOICE_STATE)
                .disableCache(CacheFlag.ACTIVITY)
                .disableIntents(GatewayIntent.DIRECT_MESSAGES)
                .disableIntents(GatewayIntent.DIRECT_MESSAGE_REACTIONS)
                .disableIntents(GatewayIntent.DIRECT_MESSAGE_TYPING)
                .setRawEventsEnabled(true)
                .setActivity(Activity.playing("Loading..."))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .addEventListeners(
                        waiter,
                        commandClient,
                        new OnReady(),
                        new OnGuildJoin(),
                        new OnGuildLeave(),
                        new OnGuildMessageReceived(),
                        new OnDisconnect()).build().awaitReady();
    }

    public static String getReleaseType() {
        if (getApi().getSelfUser().getIdLong() == 702129848910610452L) {
            return "Production";
        } else if (getApi().getSelfUser().getIdLong() == 703651420817195048L) {
            return "Alpha";
        }
        return " ";
    }

    public static EventWaiter getWaiter() {
        return waiter;
    }

    public static Map<Long, Long> getAfkUsers() {
        return afkUsers;
    }

    public static OkHttpClient getHttpClient() {
        if (client == null) {
            client = new OkHttpClient();
        }
        return client;
    }

    public static KSoftAPI getKsoftApi() {
        if (kSoftAPI == null) {
            kSoftAPI = new KSoftAPI(Config.get("ksoft"));
        }
        return kSoftAPI;
    }

    public static JDA getApi() {
        return api;
    }

    public static CommandClient getCommandClient() {
        return commandClient;
    }

    public static MongoClient getMongoClient() {
        if (mongoClient == null) {
            ConnectionString connectionString = new ConnectionString(Config.get("mongo_url"));
            mongoClient = MongoClients.create(connectionString);
            logger.info("Connected to MongoDB cache");
        }

        return mongoClient;
    }

    public static Random getRandom() {
        if (random == null) {
            random = new Random();
        }
        return random;
    }
}


