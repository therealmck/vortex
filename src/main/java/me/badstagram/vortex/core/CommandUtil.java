package me.badstagram.vortex.core;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import me.badstagram.vortex.commands.admin.Eval;
import me.badstagram.vortex.commands.admin.Health;
import me.badstagram.vortex.commands.admin.Restart;
import me.badstagram.vortex.commands.eco.income.Daily;
import me.badstagram.vortex.commands.eco.income.Hourly;
import me.badstagram.vortex.commands.fun.*;
import me.badstagram.vortex.commands.help.Invite;
import me.badstagram.vortex.commands.info.*;
import me.badstagram.vortex.commands.mod.Lock;
import me.badstagram.vortex.commands.mod.Report;
import me.badstagram.vortex.commands.mod.UnLock;
import me.badstagram.vortex.commands.mod.config.Config;
import me.badstagram.vortex.commands.mod.punish.*;
import me.badstagram.vortex.commands.role.Role;
import me.badstagram.vortex.commands.role.RoleInfo;


class CommandUtil {
    private CommandUtil() {
    }

    static CommandClientBuilder addCommands(CommandClientBuilder builder) {
        return builder
                /* ADMIN COMMANDS */
                .addCommand(new Eval())
                .addCommand(new Restart())
                .addCommand(new Health())

                /* FUN COMMANDS */
                .addCommand(new AddEmote())
                .addCommand(new AFK())
                .addCommand(new CharInfo())
                .addCommand(new DadJoke())
                .addCommand(new Embed())
                .addCommand(new Hentai())
                .addCommand(new HttpCat())
                .addCommand(new Joke())
                .addCommand(new Meme())
                .addCommand(new ProgrammerHumor())
                .addCommand(new RedditCommand())
                .addCommand(new SoftwareGore())
                .addCommand(new Slap())
                .addCommand(new GayRate())

                /* HELP COMMANDS */
                .addCommand(new Invite())

                /* INFO COMMANDS */
                .addCommand(new BotInfo())
                .addCommand(new CheckBan())
                .addCommand(new Ping())
                .addCommand(new RoleInfo())
                .addCommand(new ServerInfo())
                .addCommand(new ChannelInfo())
                .addCommand(new UserInfo())
                .addCommand(new Profile())
                .addCommand(new Donate())

                /* MOD COMMANDS */
                .addCommand(new Ban())
                .addCommand(new Clear())
                .addCommand(new ClearUser())
                .addCommand(new ForceBan())
                .addCommand(new Kick())
                .addCommand(new Mute())
                .addCommand(new TempBan())
                .addCommand(new Role())
                .addCommand(new Report())
                .addCommand(new Lock())
                .addCommand(new UnLock())
                .addCommand(new Config())

                /* ECO COMMANDS */
                .addCommand(new Daily())
                .addCommand(new Hourly());
    }
}

