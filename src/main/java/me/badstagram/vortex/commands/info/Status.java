package me.badstagram.vortex.commands.info;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class Status extends Command {
    public Status() {
        this.name = "status";
        this.help = "Gets status information about the various services used by Vortex.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        final TextChannel channel = event.getTextChannel();
        channel.sendTyping().queue();
        try {
            final String STATUS = "status";
            EmbedBuilder eb = new EmbedBuilder().setAuthor("Status", "https://www.youtube.com/watch?v=dQw4w9WgXcQ", null);
            JSONObject discordStatus = getStatus("https://srhpyqt94yxb.statuspage.io/api/v2/status.json").getJSONObject(STATUS);
            JSONObject cloudflareStatus = getStatus("https://yh6f0r4529hb.statuspage.io/api/v2/status.json").getJSONObject(STATUS);
            JSONObject galaxygateStatus = getStatus("https://statuspal.io/api/v1/status_pages/galaxygate/status").getJSONObject("status_page");
            eb.appendDescription(MarkdownUtil.maskedLink("Discord: ", "https://status.discordapp.com"))
                    .appendDescription(discordStatus.getString("description"))
                    .appendDescription("\n")
                    .appendDescription(MarkdownUtil.maskedLink("CloudFlare: ", "https://cloudflarestatus.com"))
                    .appendDescription(cloudflareStatus.getString("description"))
                    .appendDescription("\n")
                    .appendDescription(MarkdownUtil.maskedLink("GalaxyGate: ", "https://status.galaxygate.net"))
                    .appendDescription(galaxygateStatus.get("current_incident_type").equals(JSONObject.NULL) ? "All Systems Operational" : galaxygateStatus.getString("current_incident_type"))
                    .appendDescription("\n");

            channel.sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

    JSONObject getStatus(final String url) throws IOException {
        OkHttpClient client = Vortex.getHttpClient();
        Request request = new Request.Builder()
                .url(new URL(url))
                .addHeader("Accept", "application/json").build();

        Response response = client.newCall(request).execute();

        return new JSONObject(response.body().string());
    }
}


