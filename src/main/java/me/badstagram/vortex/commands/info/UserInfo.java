package me.badstagram.vortex.commands.info;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.util.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfo extends Command {
    public UserInfo() {
        this.name = "userinfo";
        this.help = "Gets info about a user.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {

        User user;
        Member m;
        EmbedBuilder eb = new EmbedBuilder();
        final List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        if (mentionedMembers.isEmpty()) {
            m = event.getMember();
        } else {
            m = mentionedMembers.get(0);
        }
        user = m.getUser();

        boolean bot = user.isBot();
        List<Role> roleList = m.getRoles();
        EnumSet<User.UserFlag> flags = m.getUser().getFlags();
        EnumSet<Permission> permissions = m.getPermissions();
        String roles = roleList.stream()
                .map(Role::getAsMention)
                .collect(Collectors.joining(", "));

        OffsetDateTime createdTime = user.getTimeCreated();
        OffsetDateTime joinedTime = m.getTimeJoined();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:yyyy KK:mm a");
        eb.setAuthor(user.getAsTag(), null, user.getAvatarUrl());
        eb.addField("Mention (id)", String.format("%s (%s) %s", user.getAsMention(), user.getId(), bot ? Constants.BOT_EMOTE : ""), false);
        eb.addField("Time Account Created", formatter.format(createdTime), false);
        eb.addField("Time Member Joined", formatter.format(joinedTime), false);

        StringBuilder sb = new StringBuilder();
        if (!flags.isEmpty()) {
            sb.append(flags.stream()
                    .map(User.UserFlag::getName)
                    .collect(Collectors.joining(", ")));
        } else {
            sb.append("None");
        }

        StringBuilder sb2 = new StringBuilder();
        if (!permissions.isEmpty()) {
            sb2.append(permissions.stream()
                    .map(Permission::getName)
                    .collect(Collectors.joining(", ")));
        } else {
            sb.append("None");
        }
        eb.addField("Flags", Utils.replaceLast(sb.toString(), ", ", ""), false);
        eb.addField("Permissions", m.isOwner() ? "Server Owner" : sb2.toString(), false);
        eb.addField("Roles", roleList.isEmpty() ? "None" : roles, false);

        event.getChannel().sendMessage(eb.build()).queue();
    }
}


