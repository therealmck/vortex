package me.badstagram.vortex.commands.info;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import java.awt.Color;

public class Ping extends Command {
    public Ping() {
        this.name = "ping";
        this.help = "Gets the bots ping to discord.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        JDA jda = Vortex.getApi();
        long gatewayPing = jda.getGatewayPing();
        event.getChannel().sendMessage("Pinging...").submit().thenCompose((message -> {
            jda.getRestPing().queue(restPing -> {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("Ping!");
                eb.addField("Gateway", MarkdownUtil.monospace(gatewayPing + "ms"), false);
                eb.addField("Rest", MarkdownUtil.monospace(restPing + "ms"), false);
                eb.setColor(new Color(255,0,255));
                message.editMessage(" ").embed(eb.build()).queue();
            });
            return null;
        }));
    }
}


