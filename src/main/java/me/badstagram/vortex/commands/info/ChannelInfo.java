package me.badstagram.vortex.commands.info;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.PermissionOverride;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ChannelInfo extends Command {
    public ChannelInfo() {
        this.name = "channelInfo";
        this.help = "Get info about a channel.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {
        TextChannel channel = event.getTextChannel();


        MessageEmbed embed = new EmbedBuilder()
                .setTitle(channel.getName())
                .addField("ID", channel.getId(), false)
                .addField("Topic", channel.getTopic() == null ? "Not Set" : channel.getTopic(), false)
                .addField("Category", channel.getParent() == null ? "None" : channel.getParent().getName(), false)
                .addField("Slowmode", channel.getSlowmode() + " Seconds", false)
                .build();

        event.reply(embed);

    }
}
