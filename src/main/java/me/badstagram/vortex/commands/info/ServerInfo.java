package me.badstagram.vortex.commands.info;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;

import java.awt.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ServerInfo extends Command {
    public ServerInfo() {
        this.name = "serverinfo";
    }

    @Override
    public void execute(CommandEvent event) {
        final Guild guild = event.getGuild();

        guild.retrieveOwner().submit()
                .thenCompose(owner -> {
                    User ownerUser = owner.getUser();
                    Set<String> guildFeatures = guild.getFeatures();
                    String features = guildFeatures.isEmpty() ? "None" : String.join(", ", guildFeatures).replaceAll("_", " ");
                    OffsetDateTime createdTime = guild.getTimeCreated();
                    int memberCount = guild.getMemberCount();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:yyyy HH:mm");



                    MessageEmbed embed = new EmbedBuilder()
                            .setTitle(guild.getName())
                            .setThumbnail(guild.getIconUrl())
                            .setColor(new Color(0,255,0))
                            .addField("Server Owner", String.format("%s (%s)", ownerUser.getAsTag(), owner.getId()), false)
                            .addField("Date Created", createdTime.format(formatter), false)
                            .addField("Member Count", String.valueOf(memberCount), false)
                            .addField("Features", features, false)
                            .build();

                    event.reply(embed);
                return null;
                });
    }
}


