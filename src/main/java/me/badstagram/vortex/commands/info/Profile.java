package me.badstagram.vortex.commands.info;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.util.DatabaseUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public class Profile extends Command {

    public Profile() {
        this.name = "profile";
        this.help = "Gets a members profile";
        this.cooldown = 10;
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = true;
    }

    @Override
    protected void execute(final CommandEvent event) {
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        Member member = mentionedMembers.isEmpty() ? event.getMember() : mentionedMembers.get(0);
        User user = member.getUser();
        List<Role> memberRoles = member.getRoles();
        EnumSet<Permission> memberPerms = member.getPermissions();
        Guild guild = event.getGuild();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:YYYY HH:mm:ss");
        String roles = "None";
        String perms = "None";
        if (member.isOwner()) {
            perms = "Server Owner";
        } else if (memberPerms.contains(Permission.ADMINISTRATOR)) {
            perms = "Administrator";
        } else if (!memberPerms.isEmpty()) {
            perms = memberPerms.stream()
                    .map(Permission::getName)
                    .collect(Collectors.joining());
        }

        if (!memberRoles.isEmpty()) {
            roles = memberRoles.stream()
                    .map(Role::getAsMention)
                    .collect(Collectors.joining(", "));
        }

        MessageEmbed embed = new EmbedBuilder()
                .setTitle(user.getAsTag())
                .setThumbnail(user.getEffectiveAvatarUrl())
                .addField("Joined on", member.getTimeJoined().format(formatter), false)
                .addField("Created at", user.getTimeCreated().format(formatter), false)
                .addField("Balance", String.format("**$%d**", DatabaseUtils.getMoney(user.getId(), guild.getId(), event)), false)
                .addField("Roles", roles, false)
                .addField("Permissions", perms, false)
                .setFooter("User ID: " + user.getId())
                .build();

        event.reply(embed);
    }
}

