package me.badstagram.vortex.commands.info;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Ban;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

public class CheckBan extends Command {
    public CheckBan() {
        this.name = "checkban";
        this.help = "Checks if a user is banned on KSoft.Si.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        try {
            EmbedBuilder eb = new EmbedBuilder();
            List<String> args = Arrays.asList(event.getArgs().split("\\s+"));

            if (args.size() != 1) {
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax.`");
                eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
                return;
            }

            KSoftAPI api = Vortex.getKsoftApi();
            Ban globalBanInfo = api.getBan().setUserId(args.get(0)).execute();

            Vortex.getApi().retrieveUserById(globalBanInfo.getModId()).submit().thenAccept(mod -> {
                if (!globalBanInfo.exists()) {
                    eb.setColor(new Color(0, 255, 0));
                    eb.setDescription("User is not global banned.");
                } else {
                    eb.setColor(new Color(255, 0, 0));
                    eb.setTitle("User is global banned.");
                    eb.addField("Ban Reason", globalBanInfo.getReason(), false);
                    eb.addField("Proof", MarkdownUtil.maskedLink("Here", globalBanInfo.getProof()), false);
                    eb.addField("Active", String.valueOf(globalBanInfo.isBanActive()), false);

                    eb.addField("Moderator", mod.getAsTag(), false);

                }
                eb.setFooter("Powered By KSoft.Si Bans.");
                event.getChannel().sendMessage(eb.build()).queue();
            });

            eb.setFooter("Powered By KSoft.Si Bans.");
            event.getChannel().sendMessage(eb.build()).queue();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}

