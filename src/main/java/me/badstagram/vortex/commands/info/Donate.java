package me.badstagram.vortex.commands.info;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.utils.MarkdownUtil;

public class Donate extends Command {
    public Donate() {
        this.name = "donate";
        this.help = "Support development of vortex!";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {
        MessageEmbed embed = new EmbedBuilder()
                .setTitle("Support")
                .addField("Patreon", MarkdownUtil.maskedLink("Coming soon :)", "https://www.youtube.com/watch?v=dQw4w9WgXcQ"), false) // haha funny :)
                .addField("GalaxyGate", MarkdownUtil.maskedLink("Support our hosting fees by using our GalaxyGate referral link", "https://billing.galaxygate.net/aff.php?aff=78"), false)
                .build();

        event.reply(embed);
    }
}
