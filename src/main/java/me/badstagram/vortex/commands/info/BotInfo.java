package me.badstagram.vortex.commands.info;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.Vortex;
import me.badstagram.vortex.util.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDAInfo;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;

public class BotInfo extends Command {
    public BotInfo() {
        this.name = "botinfo";
        this.help = "Sends information about the bot.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {

        JDA jda = Vortex.getApi();
        final EmbedBuilder embed = new EmbedBuilder();
        jda.retrieveUserById(Constants.OWNER_ID).submit().thenAccept(developer -> {

            int commandCount = event.getClient().getCommands().size();
            String jdaVersion = JDAInfo.VERSION;

            embed.setAuthor("Bot info", "https://www.youtube.com/watch?v=dQw4w9WgXcQ", event.getAuthor().getAvatarUrl())
                    .addField("Developer", developer.getAsTag(), true)
                    .addField("Command Count", String.valueOf(commandCount), true)
                    .addField("Library", "JDA", true)
                    .addField("Version", jdaVersion, true)
                    .addField("Uptime", Utils.getUptime(), false)
                    .addField("Guilds", String.valueOf(jda.getGuilds().size()), true)
                    .addField("Members", String.valueOf(jda.getUserCache().size()), true);
        });

        event.getChannel().sendMessage(embed.build()).queue();
    }
}



