package me.badstagram.vortex.commands.help;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.utils.MarkdownUtil;

public class Invite extends Command {
    public Invite() {
        this.name = "invite";
        this.help = "Gets the bots invite.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Invite");
        eb.addField("Bot Invite", MarkdownUtil.maskedLink("Here", event.getJDA().getInviteUrl(Permission.ADMINISTRATOR)), true);
        eb.addField("Support Server", MarkdownUtil.maskedLink("Here", Constants.SUPPORT_SERVER), true);

        event.getChannel().sendMessage(eb.build()).queue();
    }
}


