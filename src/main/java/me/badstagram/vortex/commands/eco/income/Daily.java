package me.badstagram.vortex.commands.eco.income;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Config;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import me.badstagram.vortex.util.Converters;
import me.badstagram.vortex.util.DatabaseUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

public class Daily extends Command {
    public Daily() {
        this.name = "daily";
        this.help = "Claim your daily reward!";
        this.cooldown = Converters.toDays(1);
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = true;
    }

    @Override
    protected void execute(final CommandEvent event) {

        ResultSet rs = null;
        MessageEmbed embed;
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement preparedStatement = con.prepareStatement("UPDATE economy SET hand = hand + ? WHERE user_id = ? AND guild_id = ? RETURNING hand")) {

            final Random rnd = Vortex.getRandom();
            final int max = Integer.parseInt(Config.get("daily"));
            final int reward = rnd.nextInt(max);

            preparedStatement.setInt(1, reward);
            preparedStatement.setString(2, event.getAuthor().getId());
            preparedStatement.setString(3, event.getGuild().getId());


            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                embed = new EmbedBuilder()
                        .setTitle("Daily Reward")
                        .setColor(new Color(0, 255, 0))
                        .setDescription(String.format("You have claimed your daily reward of **$%d**. You now have **$%d**", reward, rs.getInt("hand")))
                        .build();

                event.reply(embed);
            }

        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                ErrorHandler.handleException(e, event.getEvent());
            }
        }
    }
}

