package me.badstagram.vortex.commands.role;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Arrays;
import java.util.List;


public class AddRole extends Command {
    public AddRole() {
        this.name = "add";
        this.help = "Adds a role to a member.";
        this.botPermissions = new Permission[] {Permission.MANAGE_ROLES};
        this.userPermissions = new Permission[] {Permission.MANAGE_ROLES};
    }

    @Override
    public void execute(CommandEvent event) {

        List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        if (args.size() < 2 || mentionedMembers.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }

        Member target = mentionedMembers.get(0);
        String roleName = String.join(" ", args.subList(1, args.size()));
        final Guild guild = event.getGuild();
        List<Role> roles = guild.getRolesByName(roleName, true);
        final TextChannel channel = event.getTextChannel();
        if (roles.isEmpty()) {
            channel.sendMessage("No roles found with that name.").queue();
            return;
        }
        Role role = roles.get(0);

        final Member selfMember = event.getSelfMember();
        if (!selfMember.canInteract(role)) {
            channel.sendMessageFormat("Could not remove the `%s` role from %s", role.getName(), target.getAsMention()).queue();
            return;
        }
        guild.addRoleToMember(target, role).queue(ignored -> channel.sendMessageFormat("Added the `%s` role to %s", roleName, target.getAsMention()).queue(), thr -> ErrorHandler.handleException(thr, event.getEvent()));

    }
}

