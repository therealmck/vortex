package me.badstagram.vortex.commands.role;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;

import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public class RoleInfo extends Command {
    public RoleInfo() {
        this.name = "info";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.help = "Get info about a role.";
    }

    @Override
    protected void execute(final CommandEvent event) {

        String args = event.getArgs();
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }
        List<Role> roles = event.getGuild().getRolesByName(args, true);
        if (roles.isEmpty()) {
            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Error")
                    .setDescription("No roles found with that name.")
                    .setColor(new Color(255,0,0))
                    .build();
            event.reply(embed);
            return;
        }
        Role role = roles.get(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:yyyy HH:mm");
        String roleId = role.getId();
        String roleName = role.getName();
        String rolePermissions = "None";
        String roleCreationTime = role.getTimeCreated().format(formatter);
        String roleMembers = "None";
        boolean mentionable = role.isMentionable();
        boolean managed = role.isManaged();
        boolean hoisted = role.isHoisted();
        EnumSet<Permission> permissionList = role.getPermissions();
        List<Member> memberList = event.getGuild().getMembersWithRoles(role);

        if (!memberList.isEmpty()) {
            roleMembers = memberList.stream()
                    .map(Member::getUser)
                    .map(User::getAsMention)
                    .collect(Collectors.joining(", "));
        }

        if (permissionList.contains(Permission.ADMINISTRATOR)) {
            rolePermissions = "Administrator";
        } else if (!permissionList.isEmpty()) {
            rolePermissions = permissionList.stream()
                    .map(Permission::getName)
                    .collect(Collectors.joining(", "));
        }

        MessageEmbed embed = new EmbedBuilder()
                .setTitle(roleName)
                .setColor(role.getColor())
                .addField("ID", roleId, false)
                .addField("Creation Time", roleCreationTime, false)
                .addField("Mentionable", mentionable ? "Yes" : "No", false)
                .addField("Hoisted", hoisted ? "Yes" : "No", false)
                .addField("Managed", managed ? "Yes" : "No", false)
                .addField("Permissions", rolePermissions, false)
                .addField("Members", roleMembers, false)
                .build();

        event.reply(embed);

    }
}


