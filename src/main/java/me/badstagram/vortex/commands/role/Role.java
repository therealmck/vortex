package me.badstagram.vortex.commands.role;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Role extends Command {
    public Role() {
        this.name = "role";
        this.children = new Command[] {new AddRole(), new RemoveRole(), new RoleInfo(), new RoleColour()};
    }

    @Override
    protected void execute(final CommandEvent event) {
        String children = Arrays.stream(getChildren())
                .map(Command::getName)
                .collect(Collectors.joining("|"));

        MessageEmbed embed = new EmbedBuilder()
                .setTitle("Invalid Syntax")
                .setDescription(String.format("Usage: %s%s <%s>", event.getClient().getPrefix(), getName(), children))
                .setFooter(String.format("For support, join our support server: %s", Constants.SUPPORT_SERVER))
                .build();


        event.reply(embed);
    }
}



