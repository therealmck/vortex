package me.badstagram.vortex.commands.role;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class RoleColour extends Command {
    public RoleColour() {
        this.name = "colour";
        this.aliases = new String[]{"color"};
        this.help = "Change the colour of a role.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MANAGE_ROLES};
        this.userPermissions = new Permission[]{Permission.MANAGE_ROLES};
    }

    @Override
    protected void execute(final CommandEvent event) {
        List<String> args = Arrays.asList(event.getArgs().trim().split("\\|"));
        try {
            if (args.isEmpty()) {
                MessageEmbed embed = new EmbedBuilder()
                        .setTitle("Invalid Syntax")
                        .setDescription(String.format("%s%s <role name> ' | ' <hex colour>", event.getClient().getPrefix(), getName()))
                        .setFooter(String.format("For support join our support guild: %s", Constants.SUPPORT_SERVER))
                        .build();
                event.reply(embed);
                return;
            }

            List<Role> roles = event.getGuild().getRolesByName(args.get(0).trim(), true);

            if (roles.isEmpty()) {
                MessageEmbed embed = new EmbedBuilder()
                        .setTitle("There was an error running that command")
                        .setDescription("No roles found with that name.")
                        .setFooter(String.format("For support join our support guild: %s", Constants.SUPPORT_SERVER))
                        .build();
                event.reply(embed);
                return;
            }

            Role role = roles.get(0);

            if (!event.getSelfMember().canInteract(role)) {
                MessageEmbed embed = new EmbedBuilder()
                        .setTitle("There was an error running that command")
                        .setDescription("That role is to high for me to modify.")
                        .setFooter(String.format("For support join our support guild: %s", Constants.SUPPORT_SERVER))
                        .build();
                event.reply(embed);
                return;
            }

            role.getManager() // karen?
                    .setColor(new Color(Integer.parseInt(args.get(1).trim().replace("0x", ""), 16)))
                    .queue(v -> event.reply("Role colour changed."));

        } catch (NumberFormatException ignored) {
            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("There was an error running that command")
                    .setDescription("colour was not a valid hex code.")
                    .setFooter(String.format("For support join our support guild: %s", Constants.SUPPORT_SERVER))
                    .build();
            event.reply(embed);


        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}


