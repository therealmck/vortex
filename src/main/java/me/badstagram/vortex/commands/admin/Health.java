package me.badstagram.vortex.commands.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mongodb.lang.NonNull;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;

import javax.annotation.Nonnull;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Health extends Command {
    public Health() {
        this.name = "health";
        this.help = "Gets info about the bot's health";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(CommandEvent event) {

        if (event.getAuthor().getIdLong() != Constants.OWNER_ID) {
            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Permission Error")
                    .setDescription("You do not have permission to execute this command.")
                    .setFooter("This command is reserved for the bot owner.")
                    .setColor(new Color(255, 0, 0))
                    .build();

            event.reply(embed);
            return;
        }

        SystemInfo systemInfo = new SystemInfo();

        HardwareAbstractionLayer hal = systemInfo.getHardware();

        CentralProcessor cpu = hal.getProcessor();
        GlobalMemory ram = hal.getMemory();
        OperatingSystem os = systemInfo.getOperatingSystem();

        long totalMem = this.bytesToGigaBytes(ram.getTotal());
        long avalMem = this.bytesToGigaBytes(ram.getAvailable());
        long usedMem = totalMem - avalMem;

        event.getJDA().getRestPing().submit()
                .thenComposeAsync(rest -> {
                    long gatewayPing = event.getJDA().getGatewayPing();

                    MessageEmbed embed = new EmbedBuilder()
                            .setTitle("Vortex Health")
                            .addField("Services", "Database: `Online`", false)
                            .addField("System Information",
                                    String.format("CPU: `%s`\n" +
                                                    "RAM: `%dGB/%dGB (%dGB free)`\n" +
                                                    "OS: `%s`",
                                            cpu.getProcessorIdentifier().getName(),
                                            usedMem,
                                            totalMem,
                                            avalMem,
                                            os.getVersionInfo()), false)
                            .addField("Ping",
                                    String.format("Gateway: `%dms`\n" +
                                                    "Rest: `%dms`",
                                            gatewayPing,
                                            rest), false)
                            .addField("Fail2Ban Output", getFail2BanOutput(event), false)
                            .setFooter("Hosted by GalaxyGate", "https://i.imgur.com/N7h6XFR.png")
                            .build();

                    event.reply(embed);
                    return null;
                });
    }


    private long bytesToGigaBytes(long bytes) {
        return bytes / 1000000000;
    }

    private String getFail2BanOutput(@Nonnull final CommandEvent event) {

        try {
            Process p = new ProcessBuilder()
                    .command("/usr/bin/fail2ban-client", "status", "sshd")
                    .start();


            try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                StringBuilder lines = new StringBuilder();
                String currentLine;

                while ((currentLine = input.readLine()) != null) {
                    lines.append(currentLine).append("\n");
                }

                return MarkdownUtil.codeblock(lines.toString().replaceAll("[\\d{1,3}.*]", ""));
            }
        } catch (final Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }

        return "";
    }
}
