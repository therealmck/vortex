package me.badstagram.vortex.commands.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import groovy.lang.GroovyShell;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;

import javax.management.PersistentMBean;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Eval extends Command {
    private final GroovyShell engine = new GroovyShell();
    private static final String IMPORTS = "import java.io.*\n" +
            "import java.lang.*\n" +
            "import java.util.*\n" +
            "import java.util.concurrent.*\n" +
            "import net.dv8tion.jda.api.*\n" +
            "import net.dv8tion.jda.api.entities.*\n" +
            "import net.dv8tion.jda.api.entities.impl.*\n" +
            "import net.dv8tion.jda.api.managers.*\n" +
            "import net.dv8tion.jda.api.managers.impl.*\n" +
            "import net.dv8tion.jda.api.utils.*\n";

    public Eval() {
        this.name = "eval";
        this.help = "Runs arbitrary groovy code";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }


    @Override
    public void execute(CommandEvent event) {

        if (event.getAuthor().getIdLong() != Constants.OWNER_ID) {
            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Permission Error")
                    .setDescription("You do not have permission to execute this command.")
                    .setFooter("This command is reserved for the bot owner.")
                    .setColor(new Color(255, 0, 0))
                    .build();

            event.reply(embed);
            return;
        }


        List<String> args = Arrays.asList(event.getArgs().split("\\s+"));

        String script = event.getMessage().getContentRaw().split("\\s+", 2)[1];

        runEval(script, args, event);
    }


    void runEval(String code, List<String> args, CommandEvent event) {
        try {
            engine.setProperty("args", args);
            engine.setProperty("event", event);
            engine.setProperty("message", event.getMessage());
            engine.setProperty("channel", event.getChannel());
            engine.setProperty("jda", event.getJDA());
            engine.setProperty("guild", event.getGuild());
            engine.setProperty("member", event.getMember());
            engine.setProperty("runtime", Runtime.getRuntime());
            engine.setProperty("vortex", new Vortex());

            Object out = engine.evaluate(IMPORTS + code);
            if (out == null) {
                MessageEmbed embed = new EmbedBuilder()
                        .setTitle("Eval Success")
                        .setColor(new Color(0, 255, 0))
                        .setDescription("Code produced no output.")
                        .build();
                event.reply(embed);
                return;
            }


            String[] typeArray = out.getClass().getName().split("\\.");

            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Eval Success")
                    .setColor(new Color(0, 255, 0))
                    .addField("Output", MarkdownUtil.codeblock("java", out.toString()), false)
                    .addField("Type", MarkdownUtil.codeblock("java", typeArray[typeArray.length - 1]), false)
                    .build();

            event.getChannel().sendMessage(embed).queue();
        } catch (Exception e) {
            String[] typeArray = e.getClass().getName().split("\\.");

            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Eval Error")
                    .setColor(new Color(255, 0, 0))
                    .addField("Error Output", MarkdownUtil.codeblock("java", e.getMessage()), false)
                    .addField("Type", MarkdownUtil.codeblock("java", typeArray[typeArray.length - 1]), false)
                    .build();
            event.getChannel().sendMessage(embed).queue();
        }
    }
}

