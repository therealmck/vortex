package me.badstagram.vortex.commands.admin;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessage;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Config;
import net.dv8tion.jda.api.Permission;

import java.io.File;

public class Restart extends Command {
    public Restart() {
        this.name = "restart";
        this.help = "Restarts the bot";
        this.ownerCommand = true;
        this.hidden = true;
    }

    @Override
    public void execute(CommandEvent event) {

        event.getChannel().sendMessage("Now updating... Please wait...").queue();
        try {
            event.getJDA().shutdown();
            new ProcessBuilder()
                    .command("systemctl", "restart", "vortex")
                    .start();

        } catch (Exception e) {
            WebhookEmbed embed = new WebhookEmbedBuilder()
            .setTitle(new WebhookEmbed.EmbedTitle("Error while restarting!", null))
            .setColor(0xFF0000)
            .setDescription(e.getMessage())
            .build();

            WebhookMessageBuilder messageBuilder = new WebhookMessageBuilder();
            WebhookMessage message = messageBuilder
                    .setContent("<@424239181296959507>")
                    .addEmbeds(embed).build();

            WebhookClient client = WebhookClient.withUrl(Config.get("status_log_webhook"));

            client.send(message).thenAccept(ignored -> client.close());
        }
    }
}

