package me.badstagram.vortex.commands.admin;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessage;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Config;
import net.dv8tion.jda.api.Permission;

import java.io.File;

public class Update extends Command {
    public Update() {
        this.name = "update";
        this.help = "Updates the bot";
        this.ownerCommand = true;
        this.hidden = true;
    }

    @Override
    public void execute(CommandEvent event) {


        event.getChannel().sendMessage("Now updating... Please wait...").queue();
        try {
            event.getJDA().shutdown();
            new ProcessBuilder()
                    .directory(new File("/root/Vortex_Bot"))
                    .command("./update.sh").start();

        } catch (Exception e) {
            WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
            builder.setTitle(new WebhookEmbed.EmbedTitle("Error while updating!", null));
            builder.setColor(0xFF0000);
            builder.setDescription(e.getMessage());

            WebhookMessageBuilder messageBuilder = new WebhookMessageBuilder();
            WebhookMessage message = messageBuilder
                    .setContent("<@424239181296959507>")
                    .addEmbeds(builder.build()).build();

            WebhookClient client = WebhookClient.withUrl(Config.get("status_log_webhook"));

            client.send(message).thenAccept(ignored -> client.close());
        }
    }
}

