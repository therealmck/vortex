package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HttpCat extends Command {
    Map<Integer, String> codes;

    public HttpCat() {
        this.codes = new HashMap<>();
        this.name = "httpcat";
        this.aliases = new String[] {"http"};
        this.help = "Sends a http cat.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        List<String> args = Arrays.asList(event.getArgs().split("\\s+"));

        if (args.isEmpty()) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error running that command.");
            eb.setDescription("`Invalid syntax`");
            eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

            event.getChannel().sendMessage(eb.build()).queue();

            return;
        }
        try {
            addCodes();
            int code = Integer.parseInt(args.get(0));
            EmbedBuilder eb = new EmbedBuilder();
            if (!codes.containsKey(code)) {
                eb.setTitle("404 Not Found");
                eb.setImage("https://http.cat/404");
                event.getChannel().sendMessage(eb.build()).queue();
                return;
            }

            String message = codes.get(code);
            eb.setTitle(String.format("%d %s", code, message));
            eb.setImage("https://http.cat/"+code);

            event.getChannel().sendMessage(eb.build()).queue();
        } catch (NumberFormatException e) {
            event.getChannel().sendMessage("Invalid argument").queue(m -> m.delete().queueAfter(30, TimeUnit.SECONDS));
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

    private void addCodes() {
        codes.put(100, "Continue");
        codes.put(101, "Switching Protocols");
        codes.put(200, "OK");
        codes.put(201, "Created");
        codes.put(202, "Accepted");
        codes.put(204, "No Content");
        codes.put(206, "Partial Content");
        codes.put(207, "Multi-Status");
        codes.put(300, "Multiple choice");
        codes.put(301, "Moved Permanently");
        codes.put(302, "Found");
        codes.put(303, "See Other");
        codes.put(304, "Not Modified");
        codes.put(305, "Use Proxy");
        codes.put(307, "Temporary Redirect");
        codes.put(400, "Bad Request");
        codes.put(401, "Unauthorized");
        codes.put(402, "Payment Required");
        codes.put(403, "Forbidden");
        codes.put(404, "Not Found");
        codes.put(405, "Method Not Allowed");
        codes.put(406, "Not Acceptable");
        codes.put(408, "Request Timeout");
        codes.put(409, "Conflict");
        codes.put(410, "Gone");
        codes.put(411, "Length Required");
        codes.put(412, "Precondition Failed");
        codes.put(413, "Payload Too Large");
        codes.put(414, "Request-URI Too Long");
        codes.put(415, "Unsupported Media Type");
        codes.put(416, "Request Range Not Satisfiable");
        codes.put(417, "Expectation Failed");
        codes.put(418, "I'm A Teapot");
        codes.put(420, "Enhance Your Calm");
        codes.put(421, "Misdirected Request");
        codes.put(422, "Unprocessable Entity");
        codes.put(423, "Locked");
        codes.put(424, "Failed Dependency");
        codes.put(425, "Unordered Collection");
        codes.put(426, "Upgrade Required");
        codes.put(429, "Too Many Requests");
        codes.put(431, "Request Header Fields Too Large");
        codes.put(444, "No Response");
        codes.put(450, "Blocked by Windows Parental Controls");
        codes.put(451, "Unavailable For Legal Reasons");
        codes.put(499, "Client Closed Request");
        codes.put(500, "Internal Server Error");
        codes.put(501, "Not Implemented");
        codes.put(502, "Bad Gateway");
        codes.put(503, "Service Unavailable");
        codes.put(504, "Gateway Timeout");
        codes.put(506, "Variant Also Negotiates");
        codes.put(507, "Insufficient Storage");
        codes.put(508, "Loop Detected");
        codes.put(509, "Bandwidth Limit Exceeded");
        codes.put(510, "Not Extended");
        codes.put(511, "Network Authentication Required");
        codes.put(599, "Network Connect Timeout Error");
    }

}


