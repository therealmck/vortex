package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.internal.requests.Requester;
import okhttp3.Request;
import okhttp3.Response;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddEmote extends Command {

    public AddEmote() {
        this.name = "addemote";
        this.help = "Adds an emote into the guild.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS, Permission.MANAGE_EMOTES};
    }

    @Override
    public void execute(CommandEvent event) {

        try {
            List<String> args = Arrays.asList(event.getArgs().split("\\|"));
            EmbedBuilder eb = new EmbedBuilder();
            if (args.size() > 2) {
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax.`");
                eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
                return;
            }

            String name = args.get(0);
            String url = args.get(1).replaceAll("[<>]", "");
            Guild guild = event.getGuild();
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("User-Agent", Requester.USER_AGENT).build();
            Response response = Vortex.getHttpClient().newCall(request).execute();

            Icon imageIcon = Icon.from(response.body().bytes());

            if (guild.getEmotes().size() == guild.getMaxEmotes()) {
                event.getChannel().sendMessage(String.format("This guild already has the max emotes allowed (%d)!", guild.getMaxEmotes())).queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));
                return;
            }

            if (!guild.getEmotesByName(name, true).isEmpty()) {
                event.getChannel().sendMessage("This guild already has an emote with that name!").queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));
                return;
            }

            guild.createEmote(name, imageIcon).queue(emote -> {
                eb.setTitle("Emote Added");
                eb.addField("Name", emote.getName(), true);
                eb.addField("ID", emote.getId(), true);
                eb.addField("Animated", emote.isAnimated() ? "Yes" : "No", true);
                eb.addField("Preview", emote.getAsMention(), true);
                eb.addField("Amount Of Emotes",
                        String.valueOf(guild.getEmotes().size()),
                        true);
                event.getChannel().sendMessage(eb.build()).queue();

            }, err -> {
                eb.setTitle("There was an error running that command.");
                eb.setDescription(String.format("`%s`", err.getMessage()));
                eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
            });

        } catch (MalformedURLException e) {

            event.getChannel().sendMessage("The provided url was not valid!").queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));


        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}
