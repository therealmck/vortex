package me.badstagram.vortex.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import me.badstagram.vortex.util.DatabaseUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Slap extends Command {

    public Slap() {
        this.name = "slap";
        this.help = "Slap a member";
        this.cooldown = 10;
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = true;
    }

    @Override
    protected void execute(final CommandEvent event) {

        final int COST = 100;
        int money = DatabaseUtils.getMoney(event.getAuthor().getId(), event.getGuild().getId(), event);
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();

        if (mentionedMembers.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }
        if (money < COST) {
            event.replyError("You do not have sufficient funds for this command! Required: " + COST, msg -> msg.delete().queueAfter(30, TimeUnit.SECONDS));
            return;
        }
        User slapee = mentionedMembers.get(0).getUser();
        User slaper = event.getAuthor();

        MessageEmbed embed = new EmbedBuilder()
                .setTitle("Slap")
                .setDescription(String.format("%s gave %s a slap", slaper.getAsMention(), slapee.getAsMention()))
                .build();

        event.reply(embed);
        removeMoney(slaper.getId(), event.getGuild().getId(), COST, event);
    }

    private void removeMoney(String userId, String guildId, int amount, CommandEvent event) {
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement preparedStatement = con.prepareStatement("UPDATE economy SET hand = hand - ? WHERE guild_id = ? AND user_id = ?")){

            preparedStatement.setInt(1, amount);
            preparedStatement.setString(2, guildId);
            preparedStatement.setString(3, userId);

            preparedStatement.execute();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}

