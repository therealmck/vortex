package me.badstagram.vortex.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.net.URL;

public class Joke extends Command {
    public Joke() {
        this.name = "joke";
        this.help = "Sends a joke. Use in a NSFW channel to allow NSFW jokes";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        try {
        OkHttpClient client = Vortex.getHttpClient();
        Request request;
        Response response;
        URL url;
        if (event.getTextChannel().isNSFW()) {
            url = new URL("https://sv443.net/jokeapi/v2/joke/Any?type=twopart?blacklistFlags=racist");
        } else {
            url = new URL("https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw,racist&format=json&type=twopart");
        }

            request = new Request.Builder()
                    .url(url)
                    .addHeader("Accept", "application/json").build();

            response = client.newCall(request).execute();

            JSONObject json = new JSONObject(response.body().string());
            JSONObject flags = json.getJSONObject("flags");
            boolean nsfw = flags.getBoolean("nsfw");
            boolean religious = flags.getBoolean("religious");
            boolean racist = flags.getBoolean("racist");
            boolean sexist = flags.getBoolean("sexist");
            EmbedBuilder eb = new EmbedBuilder();

            eb.setTitle(json.getString("setup"));
            eb.setDescription(MarkdownUtil.spoiler(json.getString("delivery")));
            eb.setFooter(String.format("Flags: %s%s%s%s %nJoke from JokeAPI | Joke ID: %d | Joke Category: %s",
                    nsfw ? "nsfw" : "",
                    religious ? "religious" : "",
                    racist ? "racist" : "",
                    sexist ? "sexist" : "",
                    json.getInt("id"),
                    json.getString("category")).trim());


            event.getChannel().sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

}


