package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

public class AFK extends Command {
    public AFK() {
        this.name = "afk";
        this.help = "Marks you as AFK";
        this.botPermissions = new Permission[] {Permission.NICKNAME_MANAGE};
    }

    @Override
    public void execute(CommandEvent event) {
        Member member = event.getMember();
        final TextChannel channel = event.getTextChannel();
        if (!event.getSelfMember().canInteract(member)) {
            channel.sendMessage("I am unable to change your nickname.").queue();
            return;
        }

        String args = event.getArgs();

        String newNickname = "[AFK] ";
        if (!args.isEmpty()) {
            newNickname += args;
        } else {
            newNickname += event.getAuthor().getName();
        }

        member.modifyNickname(newNickname).queue(ignored -> channel.sendMessage("I've marked you as AFK. See you soon!").queue());
        Vortex.getAfkUsers().put(event.getGuild().getIdLong(), member.getIdLong());
    }

}

