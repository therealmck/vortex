package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.TextChannel;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

import java.util.HashMap;

public class ProgrammerHumor extends Command {
    public ProgrammerHumor() {

        this.name = "programmerhumor";
        this.aliases = new String[] {"ph"};
        this.help = "Sends a random image from r/programmerhumor.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        try {
            KSoftAPI kSoftAPI = Vortex.getKsoftApi();
            Reddit post = kSoftAPI.getRedditImage(ImageType.RANDOM_REDDIT)
                    .setSubreddit("programmerhumor")
                    .execute();

            EmbedBuilder eb = new EmbedBuilder();
            eb.setAuthor(post.getTitle(), post.getSourceUrl(), null);
            eb.setImage(post.getImageUrl());
            eb.setFooter(String.format("Posted in r/softwaregore by %s | Upvotes: %d", post.getAuthor(), post.getUpvotesInt()));

            final TextChannel channel = event.getTextChannel();
            channel.sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

}


