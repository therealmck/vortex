package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import net.dv8tion.jda.internal.requests.Requester;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.net.URL;

public class DadJoke extends Command {
    public DadJoke() {
        this.name = "dadjoke";
        this.help = "Sends a random dad joke";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        try {
            Request request;
            Response response;
            OkHttpClient client = Vortex.getHttpClient();
            request = new Request.Builder()
                    .url(new URL("https://icanhazdadjoke.com/"))
                    .addHeader("Accept", "application/json")
                    .addHeader("User-Agent", Requester.USER_AGENT).build();
            response = client.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());

            if (response.code() != 200 || json.getInt("status") != 200) {
                event.getChannel().sendMessage("Received unexpected response.").queue();
                return;
            }

            event.getChannel().sendMessage(MarkdownUtil.spoiler(json.getString("joke"))).queue();
        } catch (Exception e) {

            ErrorHandler.handleException(e, event.getEvent());
        }
    }

}


