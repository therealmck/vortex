package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Embed extends Command {
    public Embed() {
        this.name = "embed";
        this.help = "Sends an embedded message to the channel.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        List<String> args = Arrays.asList(event.getArgs().split(" \\| "));
        EmbedBuilder eb = new EmbedBuilder();

        event.getMessage().delete().queue();

        if (args.size() < 3) {
            eb.setTitle("There was an error running that command.");
            eb.setDescription("`Invalid syntax.`");
            eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

            event.getChannel().sendMessage(eb.build()).queue();

            eb.clear();
            return;
        }
        event.getMessage().delete().queue(null, ignored -> {});
        String title = args.get(0);
        Color colour = new Color(Integer.parseInt(args.get(1).replace("0x", ""), 16));
        String message = args.get(2).replace("/n", "\n");

        MessageEmbed embed = new EmbedBuilder()
                .setTitle(title)
                .setColor(colour)
                .setFooter("Requested By: "+event.getAuthor().getAsTag())
                .setDescription(message).build();


        event.getChannel().sendMessage(embed).queue();
    }

}


