package me.badstagram.vortex.commands.fun;

;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Emote;

public class CharInfo extends Command {
    public CharInfo() {
        this.name = "charinfo";
        this.help = "Get information about a character";
        this.botPermissions = new Permission[] {Permission.NICKNAME_MANAGE};
    }

    @Override
    public void execute(CommandEvent event) {
        String args = event.getArgs();
        if (args.isEmpty()) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error running that command.");
            eb.setDescription("`Invalid syntax`");
            eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

            event.getChannel().sendMessage(eb.build()).queue();

            return;
        }

        String str = args.split("\\s+", 1)[0];
        if (str.matches("<:.*:\\d+>")) {
            String id = str.replaceAll("<:.*:(\\d+)>", "$1");
            Emote emote = event.getJDA().getEmoteById(id);
            if (emote == null) {
                event.getChannel().sendMessage("Unknown emote:\n"
                        + " > ID: **" + id + "**\n"
                        + " > Guild: Unknown\n"
                        + " > URL: https://cdn.discordapp.com/" + id + ".png").queue();
                return;
            }
            event.getChannel().sendMessage("Emote **" + emote.getName() + "**:\n"
                    + " > ID: **" + emote.getId() + "**\n"
                    + " > Guild: " + (emote.getGuild() == null ? "Unknown" : "**" + emote.getGuild().getName() + "**") + "\n"
                    + " > URL: " + emote.getImageUrl()).queue();
            return;
        }
        if (str.codePoints().count() > 10) {
            event.getChannel().sendMessage("Invalid emote, or input is too long").queue();
            return;
        }
        StringBuilder builder = new StringBuilder("Emoji/Character info:");
        str.codePoints().forEachOrdered(code -> {
            char[] chars = Character.toChars(code);
            StringBuilder hex = new StringBuilder(Integer.toHexString(code).toUpperCase());
            while (hex.length() < 4)
                hex.insert(0, "0");
            builder.append("\n`\\u").append(hex).append("`   ");
            if (chars.length > 1) {
                StringBuilder hex0 = new StringBuilder(Integer.toHexString(chars[0]).toUpperCase());
                StringBuilder hex1 = new StringBuilder(Integer.toHexString(chars[1]).toUpperCase());
                while (hex0.length() < 4)
                    hex0.insert(0, "0");
                while (hex1.length() < 4)
                    hex1.insert(0, "0");
                builder.append("[`\\u").append(hex0).append("\\u").append(hex1).append("`]   ");
            }
            builder.append(String.valueOf(chars)).append("   _").append(Character.getName(code)).append("_");
        });
        event.getChannel().sendMessage(builder.toString()).queue();
    }

}


