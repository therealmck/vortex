package me.badstagram.vortex.commands.fun;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.Random;

public class GayRate extends Command {
    public GayRate() {
        this.name = "gayrate";
        this.aliases = new String[] {"howgay"};
        this.help = "Find out how gay you or another member is.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {

        Random random = Vortex.getRandom();
        String args = event.getArgs();
        String userMention = event.getAuthor().getAsMention();


        int rating = random.nextInt(100);

        MessageEmbed embed = new EmbedBuilder()
                .setTitle("Gay Rate Machine")
                .setDescription(String.format("%s is %d%% gay :rainbow_flag:", args.isEmpty() ? userMention : args, rating))
                .build();

        event.reply(embed);
    }
}
