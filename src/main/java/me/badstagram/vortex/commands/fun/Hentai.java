package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

public class Hentai extends Command {
    public Hentai() {
        this.name = "hentai";
        this.help = "Sends a hentai image";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        if (!event.getTextChannel().isNSFW()) {
            event.getChannel().sendMessage("This command can only be used in NSFW channels.").queue();
            return;
        }

        KSoftAPI api = Vortex.getKsoftApi();
        Reddit image = api.getRedditImage(ImageType.RANDOM_NSFW)
                .setSubreddit("hentai").execute();

        MessageEmbed embed = new EmbedBuilder()
                .setTitle(image.getTitle())
                .setImage(image.getImageUrl())
                .setFooter(image.getSourceUrl()).build();

        event.getChannel().sendMessage(embed).queue();
    }

}


