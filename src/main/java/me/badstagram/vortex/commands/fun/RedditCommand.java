package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.TextChannel;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

import java.util.Arrays;
import java.util.List;

public class RedditCommand extends Command {
    public RedditCommand() {
        this.name = "reddit";
        this.help = "Sends an    image from reddit.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {
        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }

        try {
            KSoftAPI kSoftAPI = Vortex.getKsoftApi();
            Reddit post = kSoftAPI.getRedditImage(ImageType.RANDOM_REDDIT)
                    .setSubreddit(args.get(0))
                    .execute();

            final TextChannel channel = event.getTextChannel();
            if (!channel.isNSFW() && post.isNsfw()) {
                channel.sendMessage("Channel must be marked as NSFW in order too post NSFW reddit posts").queue();
                return;
            }


            if (!post.subredditExists()) {
                channel.sendMessage(String.format("Subreddit `r/%s` does not exist!", args.get(0))).queue();
                return;
            }
            EmbedBuilder eb = new EmbedBuilder();
            eb.setAuthor(post.getTitle(), post.getSourceUrl(), null);
            eb.setImage(post.getImageUrl());
            eb.setFooter(String.format("Posted in %s by %s | Upvotes: %d", post.getSubreddit(), post.getAuthor(), post.getUpvotesInt()));

            channel.sendMessage(eb.build()).queue();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}


