package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

public class Meme extends Command {
    public Meme() {
        this.name = "meme";
        this.help = "Sends a meme from reddit.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        KSoftAPI kSoftAPI = Vortex.getKsoftApi();
        Reddit meme = kSoftAPI.getRedditImage(ImageType.RANDOM_MEME).
                setSubreddit("dankmemes")
                .execute();

        MessageEmbed embed = new EmbedBuilder()
                .setAuthor(meme.getTitle(), meme.getSourceUrl())
                .setImage(meme.getImageUrl())
                .setFooter(String.format("upvotes: %d | Subreddit: %s | Posted by %s", meme.getUpvotesInt(), meme.getSubreddit(), meme.getAuthor()))
                .build();
        event.getChannel().sendMessage(embed).queue();
    }

}


