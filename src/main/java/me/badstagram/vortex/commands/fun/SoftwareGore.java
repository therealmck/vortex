package me.badstagram.vortex.commands.fun;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

public class SoftwareGore extends Command {
    public SoftwareGore() {
        this.name = "softwaregore";
        this.help = "Sends a post from r/softwaregore.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    protected void execute(final CommandEvent event) {
        try {
            KSoftAPI kSoftAPI = Vortex.getKsoftApi();
            Reddit post = kSoftAPI.getRedditImage(ImageType.RANDOM_REDDIT)
                    .setSubreddit("softwaregore")
                    .execute();

            MessageEmbed embed = new EmbedBuilder()
                    .setAuthor(post.getTitle(), post.getSourceUrl(), null)
                    .setImage(post.getImageUrl())
                    .setFooter(String.format("Posted in r/softwaregore by %s | Upvotes: %d", post.getAuthor(), post.getUpvotesInt()))
                    .build();

            event.reply(embed);
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}


