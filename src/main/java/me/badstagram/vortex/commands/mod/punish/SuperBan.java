package me.badstagram.vortex.commands.mod.punish;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.User;
import net.explodingbush.ksoftapi.KSoftAPI;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class SuperBan extends Command {

    public SuperBan() {
        this.name = "superban";
        this.help = "Report a user to KSoft.Si.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {

        
        KSoftAPI ksoft = Vortex.getKsoftApi();
        User moderator = event.getAuthor();
        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        List<String> argsPipe = Arrays.asList(String.join(" ", args).split(" \\| "));
        if (argsPipe.size() < 3) {
            ErrorHandler.invalidSyntax(event);
            return;
        }
        event.getJDA().retrieveUserById(args.get(0)).submit().whenComplete(((user, throwable) -> {
            if (throwable != null) {
                event.getChannel().sendMessage(throwable.getMessage().replaceAll("\\d{5}:", "")).queue();
            }

            String offenderId = user.getId();
            String offenderName = user.getName();
            String offenderDiscrim = user.getDiscriminator();

            String modId = moderator.getId();

            String reason = String.join(" ", args.subList(2, args.size()));

            Pattern imgurImage = Pattern.compile("^https?://i\\.imgur\\.com/(\\w+)\\.(?:jpg|png)$");
            Pattern imgurAlbum = Pattern.compile("^https?://imgur\\.com/a/(\\w+)$");

            if (!imgurImage.matcher(args.get(1)).find() || !imgurAlbum.matcher(args.get(1)).find()) {
                ErrorHandler.invalidSyntax(event);
                return;
            }

            ksoft.getBan().addBan()
                    .setUserId(offenderId)
                    .setUsername(offenderName)
                    .setDiscriminator(offenderDiscrim)
                    .setProof(args.get(1))
                    .setReason(reason)
                    .setModeratorId(modId)
                    .set().execute();
        }));

        event.getChannel().sendMessage("KSoft.Si global ban reported!").queue();
    }
}



