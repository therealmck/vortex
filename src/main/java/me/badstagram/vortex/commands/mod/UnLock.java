package me.badstagram.vortex.commands.mod;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;

public class UnLock extends Command {
    public UnLock() {
        this.name = "unlock";
        this.help = "Unlocks the channel this command was executed in";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MANAGE_CHANNEL};
        this.userPermissions = new Permission[]{Permission.MANAGE_CHANNEL};
    }

    @Override
    protected void execute(final CommandEvent event) {
        event.getTextChannel()
                .upsertPermissionOverride(event.getGuild().getPublicRole())
                .setAllow(Permission.MESSAGE_WRITE)
                .submit()
                .whenComplete((v, err) -> {
                    MessageEmbed embed = new EmbedBuilder()
                            .setTitle("Channel Unlocked")
                            .setDescription(String.format(":unlock: %s has been unlocked by %s", event.getTextChannel().getAsMention(), event.getAuthor().getAsMention()))
                            .build();
                    event.reply(embed);
                });
    }
}
