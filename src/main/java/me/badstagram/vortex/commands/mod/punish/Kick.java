package me.badstagram.vortex.commands.mod.punish;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Arrays;
import java.util.List;

public class Kick extends Command {
    public Kick() {
        this.name = "kick";
        this.help = "Kicks a member from the guild.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
        final List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        if (mentionedMembers.isEmpty() || args.size() < 2) {
            ErrorHandler.invalidSyntax(event);
            return;
        }


        String reason = String.join(" ", args.subList(1, args.size()));
        Member target = mentionedMembers.get(0);

        final Member member = event.getMember();
        final Member selfMember = event.getSelfMember();
        final TextChannel channel = event.getTextChannel();
        if (!member.canInteract(target) || !selfMember.canInteract(target)) {
            channel.sendMessageFormat(":x: **%s** could not be kick.", target.getUser().getAsTag()).queue();
            return;
        }

        final Guild guild = event.getGuild();
        target.getUser().openPrivateChannel().submit()
                .thenAcceptAsync(ch -> ch.sendMessageFormat("You have been kicked from **%s** by **%s** for **%s**", guild.getName(), member.getUser().getAsTag(), reason).queue())
                .whenCompleteAsync((ignored_, ignored__) -> guild.kick(target).reason(reason).submit()
                        .thenAcceptAsync(v -> channel.sendMessageFormat(":white_check_mark: **%s** (`%s`) has been kicked: `%s`", target.getUser().getAsTag(), target.getId(), reason).queue())
                        .exceptionally(thr -> {
                            channel.sendMessageFormat(":x: **%s** could not be kicked: `%s`", target.getUser().getAsTag(), thr.getMessage()).queue();
                            return null;
                        }));
    }
}


