package me.badstagram.vortex.commands.mod.punish;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.*;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClearUser extends Command {
    public ClearUser() {
        this.name = "clearuser";
        this.help = "Clears a users messages.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public void execute(CommandEvent event) {
//                          0          1
        //clearuser 696978475692589126 4

        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }
        int amount;

        final TextChannel channel = event.getTextChannel();
        try {
            if (args.size() == 2) {
                amount = Integer.parseInt(args.get(1));
            } else {
                amount = 100;
            }


            if (amount < 2 || amount > 100) {
                channel.sendMessage("Input must be between 2 and 100.").queue();
                return;
            }
            long userId = Long.parseLong(args.get(0));
            channel.getIterableHistory()
                    .takeAsync(amount)
                    .thenApplyAsync(messages -> {
                        List<Message> goodMessages = messages.stream()
                                .filter(m -> m.getTimeCreated().isBefore(OffsetDateTime.now().plus(2, ChronoUnit.WEEKS)) &&
                                        m.getAuthor().getIdLong() == userId)
                                .collect(Collectors.toList());

                        channel.purgeMessages(goodMessages);
                        return goodMessages.size();
                    })
                    .whenCompleteAsync((count, thr) -> {
                        EmbedBuilder eb = new EmbedBuilder();
                        eb.setTitle("User Cleared");
                        eb.setDescription(String.format("**%d** messages by <@%s> in <#%s> was cleared by %s", count, userId, channel.getId(), event.getAuthor().getAsMention()));
                        eb.setColor(new Color(78, 93, 148));
                        channel.sendMessage(eb.build()).queue();
                    })
                    .exceptionally(thr -> {
                        if (thr != null) {
                            channel.sendMessage(thr.getMessage()).queue();
                        }
                        return 0;
                    });

        } catch (NumberFormatException ignored) {
            channel.sendMessage("Input was not a valid number.").queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }

    }

}


