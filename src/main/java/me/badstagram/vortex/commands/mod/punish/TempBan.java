package me.badstagram.vortex.commands.mod.punish;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.enums.PunishmentType;
import me.badstagram.vortex.util.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class TempBan extends Command {
    public TempBan() {
        this.name = "tempban";
        this.help = "Temp-bans a user from the server.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.BAN_MEMBERS};
        this.userPermissions = new Permission[] {Permission.BAN_MEMBERS};
    }

    @Override
    public void execute(CommandEvent event) {
        try {


            //          0       1       2
            // v!ban <@user> <Time> <reason>
            final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
            final List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
            if (args.size() < 3 || mentionedMembers.isEmpty()) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax`");
                eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                return;
            }

            Member moderator = event.getMember();
            Member offender = mentionedMembers.get(0);
            String reason = String.join(" ", args.subList(2, args.size()));

            if (!moderator.canInteract(offender) || !event.getGuild().getSelfMember().canInteract(offender)) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`That member can not be banned.`");
                eb.setFooter(String.format("For support, join our support guild: %s", Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                return;
            }
            final String time = args.subList(1, args.size()).get(0);


            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(new Color(255, 0, 0));
            eb.setTitle("Banned");
            eb.appendDescription(String.format("**Server**: %s%n", event.getGuild().getName()));
            eb.appendDescription(String.format("**Banned By**: %s%n", moderator.getUser().getAsTag()));
            eb.appendDescription(String.format("**Reason**: %s%n", reason));

            final TextChannel channel = event.getTextChannel();
            offender.getUser().openPrivateChannel().submit().thenCompose(privateChannel -> channel.sendMessage(eb.build()).submit().thenCompose(ignored -> {
                Utils.addPunishmentToDb(offender.getId(), moderator.getId(), reason, event.getGuild().getId(), PunishmentType.BAN, time, event.getEvent());
                return null;
            })).whenComplete((ignored, ign) ->
                    event.getGuild().ban(offender, 7).reason(reason).queue(ignored_ -> {
                        eb.clear();
                        eb.setColor(new Color(255, 0, 0));
                        eb.setTitle("User Banned");
                        eb.appendDescription(String.format("**User**: %s (%s)%n", offender.getUser().getAsTag(), offender.getId()));
                        eb.appendDescription(String.format("**Banned By**: %s%n", moderator.getUser().getAsTag()));
                        eb.appendDescription(String.format("**Duration**: %s%n", time));
                        eb.appendDescription(String.format("**Reason**: %s%n", reason));

                        event.getChannel().sendMessage(eb.build()).queue();
                    }));
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

}


