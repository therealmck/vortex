package me.badstagram.vortex.commands.mod.punish;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Arrays;
import java.util.List;

public class Ban extends Command {
    public Ban() {
        this.name = "ban";
        this.help = "Bans a member from the guild.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS, Permission.BAN_MEMBERS};
        this.userPermissions = new Permission[] {Permission.BAN_MEMBERS};
    }

    @Override
    public void execute(CommandEvent event) {
        final List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        if (mentionedMembers.isEmpty() || args.size() < 2) {
            ErrorHandler.invalidSyntax(event);
            return;
        }


        String reason = String.join(" ", args.subList(1, args.size()));
        Member target = mentionedMembers.get(0);

        if (!event.getMember().canInteract(target) || !event.getSelfMember().canInteract(target)) {
            event.replyFormatted(":x: **%s** could not be banned.", target.getUser().getAsTag());
            return;
        }

        final Guild guild = event.getGuild();
        final Member member = event.getMember();
        final TextChannel channel = event.getTextChannel();
        target.getUser().openPrivateChannel().submit()
                .thenAcceptAsync(ch -> ch.sendMessageFormat("You have been banned from **%s** by **%s** for **%s**", guild.getName(), member.getUser().getAsTag(), reason).queue())
                .whenCompleteAsync((ignored_, ignored__) -> guild.ban(target, 7).reason(reason).submit()
                        .thenAcceptAsync(v -> channel.sendMessageFormat(":white_check_mark: **%s** (`%s`) has been banned: `%s`", target.getUser().getAsTag(), target.getId(), reason).queue())
                        .exceptionally(thr -> {
                            channel.sendMessageFormat(":x: **%s** could not be banned: `%s`", target.getUser().getAsTag(), thr.getMessage()).queue();
                            return null;
                        }));
    }
}



