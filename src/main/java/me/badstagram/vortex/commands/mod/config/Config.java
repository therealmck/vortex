package me.badstagram.vortex.commands.mod.config;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.enums.ConfigOption;
import me.badstagram.vortex.util.DatabaseUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Config extends Command {
    private static final String ROLE_MENTION = "<@&%s>";
    private static final String CHANNEL_MENTION = "<#%s>";

    public Config() {
        this.name = "configure";
        this.aliases = new String[]{"config"};
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.userPermissions = new Permission[]{Permission.MANAGE_SERVER};
        this.children = new Command[]{new Set()};
    }

    @Override
    protected void execute(final CommandEvent event) {


        MessageEmbed embed = new EmbedBuilder()
                .setTitle("Current Config")
                .addField("Muted Role", String.format(ROLE_MENTION, getConfig(event.getGuild(), ConfigOption.MUTED_ROLE, event.getEvent())), false)
                .addField("Mod Log", String.format(CHANNEL_MENTION, getConfig(event.getGuild(), ConfigOption.MOD_LOG, event.getEvent())), false)
                .addField("Punish Log", String.format(CHANNEL_MENTION, getConfig(event.getGuild(), ConfigOption.PUNISH_LOG, event.getEvent())), false)
                .setFooter(String.format("To set an option, use %sconfigure set <option> <value>", event.getClient().getPrefix()))
                .build();

        event.reply(embed);
    }

    private String getConfig(final Guild guild, final ConfigOption configOption, final MessageReceivedEvent event) {

        String query = "SELECT * FROM guild_config WHERE guild_id = ?";
        ResultSet rs = null;
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(query)) {

            preparedStatement.setString(1, guild.getId());

            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                switch (configOption) {
                    case MOD_LOG:
                        return rs.getString("mod_log");
                    case MUTED_ROLE:
                        return rs.getString("muted_role");
                    case PUNISH_LOG:
                        return rs.getString("punish_log");
                }
            }
        } catch (Exception e) {
            ErrorHandler.handleException(e, event);
            return "Error";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                ErrorHandler.handleException(e, event);
            }
        }
        return "You done broke it"; // idk what the fuck happened but this shouldn't have happened
    }
}
