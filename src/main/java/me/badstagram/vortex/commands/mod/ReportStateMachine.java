package me.badstagram.vortex.commands.mod;

import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.explodingbush.ksoftapi.KSoftAPI;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

public class ReportStateMachine extends ListenerAdapter{

    /* states */
    private static final int WAITING_FOR_USER_ID = 1;
    private static final int WAITING_FOR_REASON = 2;
    private static final int WAITING_FOR_PROOF = 3;
    private static final int FINAL_STATE = 5;

    private final JDA JDA;
    /* checks */
    private final long USER_ID;
    private final long CHANNEL_ID;
    private final long GUILD_ID;
    private int state; // current state

    /* report details*/
    private String userId, reason, proof;
    private boolean userIdValid, proofValid;

    public ReportStateMachine(JDA jda, long userId, long channelId, long guildId) {
        jda.addEventListener(this);
        this.JDA = jda;
        this.USER_ID = userId;
        this.CHANNEL_ID = channelId;
        this.GUILD_ID = guildId;
        this.state = WAITING_FOR_USER_ID;
    }

    @Override
    public void onGuildMessageReceived(@Nonnull final GuildMessageReceivedEvent event) {
        if (event.getAuthor().getIdLong() != USER_ID) return;
        if (event.getChannel().getIdLong() != CHANNEL_ID) return;
        if (event.getGuild().getIdLong() != GUILD_ID) return;

        User user = null;
        Message message = event.getMessage();
        String content = message.getContentRaw();

        if (content.equalsIgnoreCase("cancel")) {
            message.getChannel().sendMessageFormat("%s Cancelled", Vortex.getCommandClient().getError()).queue();
            JDA.removeEventListener(this);
            return;
        }

        switch (state) {
            case WAITING_FOR_USER_ID:
                try {
                    user = JDA.retrieveUserById(content).complete();

                    if (user == null) {
                        message.getChannel().sendMessage("Unknown User. Report has been canceled.").queue();
                        cleanUp();
                        return;

                    }
                } catch (NumberFormatException ex) {
                    message.getChannel().sendMessage("Unknown User. Report has been canceled.").queue();
                    cleanUp();
                    return;
                }


                userId = content;
                message.getChannel().sendMessage("Why do you wish to report this user?").queue();
                state = WAITING_FOR_REASON;
                break;

            case WAITING_FOR_REASON:
                reason = content;
                state = WAITING_FOR_PROOF;
                message.getChannel().sendMessage("Please provide proof. Note the proof must be an imgur URL").queue();
                break;

            case WAITING_FOR_PROOF:
                final Pattern imgurUrl = Pattern.compile("https?://i\\.imgur\\.com/(\\w+)\\.(?:jpg|png)$", Pattern.CASE_INSENSITIVE);
                final Pattern imgurAlbum = Pattern.compile("^https?://imgur\\.com(?:/a)?/(\\w+)$", Pattern.CASE_INSENSITIVE);


                if (!imgurAlbum.matcher(content).matches() || !imgurUrl.matcher(content).matches()) {
                    message.getChannel().sendMessage("Proof was not a valid imgur URL. Report has been canceled.").queue();
                    cleanUp();
                    return;
                }


                proof = content.replace("<", "").replace(">", "");
                state = FINAL_STATE;
                break;

            case FINAL_STATE:
                event.getChannel().sendMessage("User reported! Thank you for keeping discord a safe place! This is a service provided by KSoft.Si.").queue();

                JDA.retrieveUserById(this.userId).submit()
                        .thenCompose(u -> {
                            final KSoftAPI kSoft = Vortex.getKsoftApi();
                            kSoft.getBan().addBan()
                                    .setUserId(userId)
                                    .setReason(reason)
                                    .setAppealable(true)
                                    .setModeratorId(String.valueOf(USER_ID))
                                    .setUsername(u.getName())
                                    .setDiscriminator(u.getDiscriminator())
                                    .setProof(proof)
                                    .set()
                                    .execute();
                            return null;
                        });

                JDA.removeEventListener(this);
                break;

            default:
                message.getChannel().sendMessage("Uhh... I dont know what the fuck you did, but this should never have been shown. Try again maybe?").queue();
                cleanUp();
        }
    }

    private void cleanUp() {
        JDA.removeEventListener(this);
    }
}
