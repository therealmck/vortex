package me.badstagram.vortex.commands.mod.config;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.util.DatabaseUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;

public class Set extends Command {
    public Set() {
        this.name = "set";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.userPermissions = new Permission[]{Permission.MANAGE_SERVER};
    }

    @Override
    protected void execute(final CommandEvent event) {
        List<String> args = Arrays.asList(event.getArgs().split("\\s+"));

        if (args.isEmpty() || args.size() < 2) {
            MessageEmbed embed = new EmbedBuilder()
                    .setTitle("Oops, something went wrong while setting the config")
                    .setDescription("Invalid syntax.")
                    .setFooter("Use channel/role IDs and replace spaces with underscores (_)")
                    .build();
            event.reply(embed);
            return;
        }

        // PUSH CONFIG TO DATABASE
        List<String> validOpts = Arrays.asList("punish_log", "mod_log", "muted_role");



            if (!validOpts.contains(args.get(0))) {
                MessageEmbed embed = new EmbedBuilder()
                        .setTitle("Oops, something went wrong while setting the config")
                        .setDescription("Invalid syntax.")
                        .setFooter("Use channel/role IDs and replace spaces with underscores (_)")
                        .build();
                event.reply(embed);
                return;
            }

        String query = "UPDATE guild_config SET " + args.get(0) + " = ? WHERE guild_id = ?";
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement preparedStatement = con.prepareStatement(query)) {


            preparedStatement.setString(1, args.get(1));
            preparedStatement.setString(2, event.getGuild().getId());

            preparedStatement.execute();
            event.reply("Config set!");

        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }
}
