package me.badstagram.vortex.commands.mod;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class Report extends Command {
    public Report() {
        this.name = "report";
        this.help = "Reports a global ban on KSoft.Si";
    }

    @Override
    protected void execute(final CommandEvent event) {

        event.reply("Who do you wish to report. Enter their user ID");
        new ReportStateMachine(event.getJDA(), event.getAuthor().getIdLong(), event.getChannel().getIdLong(), event.getGuild().getIdLong());
    }
}
