package me.badstagram.vortex.commands.mod.punish;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Arrays;
import java.util.List;

public class ForceBan extends Command {
    public ForceBan() {
        this.name = "forceban";
        this.help = "Bans a user from the guild using their ID.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }


    @Override
    public void execute(CommandEvent event) {

        final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event);
            return;
        }
        String reason = String.join(" ", args.subList(1, args.size()));
        long userId = 0L;

        final TextChannel channel = event.getTextChannel();
        try {
            userId = Long.parseLong(args.get(0));
        } catch (NumberFormatException e) {
            channel.sendMessage("User ID was not in the correct format.").queue();
        }

        final JDA jda = Vortex.getApi();
        final Guild guild = event.getGuild();
        jda.retrieveUserById(userId).submit()
                .thenAcceptAsync(user -> guild.ban(user, 7).reason("[Force Ban] " + reason).submit()
                        .thenAcceptAsync(v -> channel.sendMessageFormat(":white_check_mark: %s (`%s`) has been banned: `%s`", user.getAsTag(), user.getId(), reason).queue()))
                .exceptionally(thr -> {
                            ErrorHandler.handleException(thr, event.getEvent());
                            return null;
                });
    }
}

