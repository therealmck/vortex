package me.badstagram.vortex.commands.mod.punish;


import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import me.badstagram.vortex.core.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.*;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Clear extends Command {
    public Clear() {
        this.name = "clear";
        this.help = "Clears messages from a channel.";
        this.botPermissions = new Permission[] {Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_MANAGE};
    }

    @Override
    public void execute(CommandEvent event) {

        final TextChannel channel = event.getTextChannel();
        try {
            int messagesToClear;
            final List<String> args = Arrays.asList(event.getArgs().split("\\s+"));
            if (args.isEmpty()) {
                messagesToClear = 100;
            } else {
                messagesToClear = Integer.parseInt(args.get(0)) + 1;
            }

            if (messagesToClear < 2 || messagesToClear > 100) {
                channel.sendMessage("Input must be between 2 and 100.").queue();
                return;
            }


            channel.getIterableHistory()
                    .takeAsync(messagesToClear)
                    .thenApplyAsync(messages -> {
                        List<Message> goodMessages = messages.stream()
                                .filter(m -> m.getTimeCreated().isBefore(OffsetDateTime.now().plus(2, ChronoUnit.WEEKS))).collect(Collectors.toList());

                        channel.purgeMessages(goodMessages);
                        return goodMessages.size();
                    })
                    .whenCompleteAsync((count, thr) -> {
                        EmbedBuilder eb = new EmbedBuilder();
                        eb.setTitle("Channel Cleared");
                        eb.setDescription(String.format("**%d** messages was cleared by %s", count, event.getAuthor().getAsMention()));
                        eb.setColor(new Color(78, 93, 148));
                        channel.sendMessage(eb.build()).queue();
                    })
                    .exceptionally(thr -> {
                        if (thr != null) {
                            channel.sendMessage(thr.getMessage()).queue();
                        }
                        return 0;
                    });


        } catch (NumberFormatException ignored) {
            channel.sendMessage("Input was not a valid number.").queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event.getEvent());
        }
    }

}


