package me.badstagram.vortex.commands.mod.punish;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.Permission;

public class Mute extends Command {
    public Mute() {
        this.name = "mute";
        this.help = "Mutes a member.";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    protected void execute(final CommandEvent event) {
        throw new UnsupportedOperationException();
    }
}


