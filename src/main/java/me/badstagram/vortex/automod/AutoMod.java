package me.badstagram.vortex.automod;

import me.badstagram.vortex.core.Config;
import me.badstagram.vortex.core.ErrorHandler;
import me.badstagram.vortex.core.Vortex;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import okhttp3.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class AutoMod {
    public static void moderateAttachments(List<Message.Attachment> attachments, GuildMessageReceivedEvent event) {
        if (attachments.isEmpty()) return;


        try {
            if (checkAttachments(attachments)) {
                event.getMessage().delete().queue();
                event.getChannel().sendMessageFormat("%s, Don't post porn.", event.getAuthor().getAsMention()).queue();
            }


        } catch (final IOException e) {
            ErrorHandler.handleException(e, event);
        }


    }

    private static boolean checkAttachments(List<Message.Attachment> attachments) throws IOException {
        OkHttpClient client = Vortex.getHttpClient();
        for (Message.Attachment attachment : attachments) {
            String url = attachment.getUrl();
            HttpUrl httpUrl = new HttpUrl.Builder()
                    .scheme("https")
                    .host("www.picpurify.com")
                    .addPathSegment("analyse")
                    .addPathSegment("1.1")
                    .addQueryParameter("API_KEY", Config.get("picpurify"))
                    .addQueryParameter("task", "porn_moderation")
                    .addQueryParameter("url_image", url)
                    .build();


            RequestBody body = RequestBody.create(null, new byte[0]);

            Request request = new Request.Builder()
                    .url(httpUrl)
                    .post(body)
                    .build();


            Response response = client.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());

            return json.getJSONObject("porn_moderation").getBoolean("porn_content");
        }
        return false;
    }


}
